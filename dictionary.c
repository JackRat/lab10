#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

// Load the dictionary of words from the given filename.
// Return a pointer to the array of strings.
// Sets the value of size to be the number of valid
// entries in the array (not the total array length).
char ** loadDictionary(char *filename, int *size)
{
	FILE *in = fopen(filename, "r");
	if (!in)
	{
	    perror("Can't open dictionary");
	    exit(1);
	}
	
	// TODO
	// Allocate memory for an array of strings (arr).
    char **words = malloc(10 * sizeof(char *));
    char line[100];
    int wordcount = 0;
    while(fgets(line, 20, in))
    {
        char *word = malloc(strlen(line)+1);
        strcpy(word, line);
        words[wordcount] = word;
        wordcount++;

        //Trim Newline
        char *nl = strchr(line, '\n');
        if (nl) *nl = '\0';

	    // Expand array if necessary (realloc).
        if(wordcount == *size)
        {
            //Makes array bigger
            size += 10;
            words = realloc(words, (*size * sizeof(char *)));
            
        }
	    // Allocate memory for the string (str).
	    // Copy each line into the string (use strcpy).
	    // Attach the string to the large array (assignment =).
    }
    fclose(in);
    //printf("I Made it here");
    for(int i = 0; i < wordcount; i++)
    {
        printf("%d", wordcount);
    }
	// The size should be the number of entries in the array.
	*size = wordcount;
	
	// Return pointer to the array of strings.
	return words;
    
}

// Search the dictionary for the target string
// Return the found string or NULL if not found.
char * searchDictionary(char *target, char **dictionary, int size)
{
    if (dictionary == NULL) return NULL;
    
	for (int i = 0; i < size; i++)
	{
	    if (strcmp(target, dictionary[i]) == 0)
	    {
	        return dictionary[i];
	    }
	}
	return NULL;
}

int main(int argc, char *argv[])
{
    int size = (int)argv[2];
    loadDictionary(argv[1], &size);
}